/**
 * @author Rodrigo Rivas 1910674 and Kirill Parhomenco 1933830
 */
'use strict';

document.addEventListener('DOMContentLoaded',setup);

let glob = {};

/**
 * Sets up the environment by creating global vartiables and event listeners
 * @author Rodrigo, Kirill
 */
function setup(){
    glob.interval;
    glob.indexWord=0;
    glob.before = document.querySelector('#char-before-focus');
    glob.focus = document.querySelector('#char-focus');
    glob.after = document.querySelector('#char-remaining');
    glob.wpm = document.querySelector('#speed');
    glob.wpm.addEventListener('click',setSpeed);
    checkWpm();

    glob.btn = document.querySelector('#start');
    glob.btn.addEventListener('click',start);

}
/**
 * Function responsible for starting the reading; invoked when the button is pressed.
 * @author Rodrigo, Kirill
 */
function start(){
    let txt = glob.btn.innerText;
    if(txt == 'Stop'){
        clearInterval(glob.interval);
        glob.btn.innerText ='Start';
        glob.indexWord = 0;
    }else{
        getNextQuote();
        glob.btn.innerText = 'Stop';
    }
}
/**
 * Uses fetch to get quote from Ron Swanson API 
 * if ok, splits it
 * then displays it
 * Error if there was a problem fetching.
 * @author Rodrigo
 */
function getNextQuote(){
    let url = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';
    fetch(url)
        .then(response =>{
            if(response.ok)
                return response.json();
            throw new Error("Status code: " + response.statusCode);
        })
        .then(quote => stringSplitter(quote))
        .then(splited => displayQuote(splited))
        .catch(error => treatError(error));
}
/**
 * Error handler
 * @param {error from api fetch} error 
 * @author Rodrigo
 */
function treatError(error){
    glob.p.textContent = error+", Please try again";
    console.log(error);
}
/**
 * Splits the quote to use in the reader. Uses split() funtion with whitespace as separator.
 * @param {String} quote fetched from the internet
 * @author Rodrigo
 */
function stringSplitter(quote){
    let splited = quote[0].split(' ');
    return splited;
}
/**
 * Responsible for displaying the string - uses displayWord on each of the word in the array split by stringSplitter
 * and sets the speed interval
 * @param {String array} strArr consists of separate words from fetched quote
 * @author Rodrigo
 */
function displayQuote(strArr){
    let delay = 60000/glob.wpm.value;
    console.log(strArr);
    glob.interval = setInterval(displayWord,delay,strArr);
}
/**
 * Displays a given word in strArr in 3 pieces, middle piece is highlighted and alligned
 * @param {String array} strArr array of strings from stringSplitter
 * @author Kirill, Rodrigo
 */
function displayWord(strArr){
    console.log(glob.indexWord); // not necesary, just for us to see it working in the console   
    if(glob.indexWord < strArr.length){
        let wordArr = wordSplitter(strArr[glob.indexWord]);
        glob.before.innerText = wordArr[0];
        glob.focus.innerText = wordArr[1];
        glob.after.innerText = wordArr[2];
        glob.indexWord++;
    }else{
        glob.indexWord =0;
        clearInterval(glob.interval);
        getNextQuote();
    }
}
/**
 * Using the logic behind fast reading, indents a word and splits it based on its length
 * https://en.wikipedia.org/wiki/Rapid_serial_visual_presentation
 * @param {String} word current word in the splitString array
 * @author Kirill, Rodrigo
 */
function wordSplitter(word){
    let wlngt = word.length;
    if(wlngt ==  1){
        return ['    ', word,''];
    }
    else if(wlngt > 1 && wlngt <= 5){
        return ['   '+ word.substring(0,1),word.substring(1,2),word.substring(2,wlngt)];
    }
    else if(wlngt >5 && wlngt <=9){
        return ['  '+ word.substring(0,2),word.substring(2,3),word.substring(3,wlngt)];
    }
    else if(wlngt >9 && wlngt <=13){
        return [' '+ word.substring(0,3),word.substring(3,4),word.substring(4,wlngt)];
    }
    else{
        return [word.substring(0,4),word.substring(4,5),word.substring(5,wlngt)];
    }

}
/**
 * Setting the speed of reading
 * @author Rodrigo
 */
function setSpeed(){
    localStorage.setItem('wpm',glob.wpm.value);
}
/**
 * checking speed setter input field
 * @author Rodrigo
 */
function checkWpm(){
    if(localStorage.getItem('wpm') == null){
            glob.wpm.value = 100;
            setSpeed();
    }else{
        glob.wpm.value = localStorage.getItem('wpm');
    }
}